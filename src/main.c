#include <gtk/gtk.h>
#include <stdlib.h>
#include <sqlite3.h>

GtkWidget *tree_view_left, *tree_view_right;
sqlite3 *ppDb = NULL;
char *errmsg = NULL;

static void select_random_node(GtkTreeModel* tree_model);
int select_callback(void *arg, int argc, char **argv, char **column);

G_MODULE_EXPORT void signal_destroy(GtkWidget *widget, gpointer user_data) {
	gtk_main_quit();
}

static void process_node(GtkWidget* tree_view_win, GtkWidget* tree_view_lose) {
	gint win;
	gchar* name;
	gint rowid;
	GtkTreeModel* tree_model;
	GtkTreeIter iter;
	GtkTreeSelection* tree_selection_right = gtk_tree_view_get_selection(GTK_TREE_VIEW(tree_view_win));
	gtk_tree_selection_get_selected(tree_selection_right, &tree_model, &iter);
	gtk_tree_model_get(tree_model, &iter, 0, &win, 1, &name, -1);

	GtkTreeSelection* tree_selection_left = gtk_tree_view_get_selection(GTK_TREE_VIEW(tree_view_lose));
	gtk_tree_selection_get_selected(tree_selection_left, &tree_model, &iter);
	gtk_tree_model_get(tree_model, &iter, 0, &rowid, -1);
	gtk_list_store_set(GTK_LIST_STORE(tree_model), &iter, 2, win, -1);

	char* sql = sqlite3_mprintf("UPDATE node SET win = %d WHERE rowid = %d;", win, rowid);
	g_print("%s\n", sql);
	sqlite3_exec(ppDb, sql, NULL, NULL, &errmsg);
	sqlite3_free(sql);

	gtk_list_store_remove(GTK_LIST_STORE(tree_model), &iter);

	gint length = gtk_tree_model_iter_n_children(tree_model, NULL);

	if (length > 1) {
		select_random_node(tree_model);
	} else {
		sql = sqlite3_mprintf("INSERT INTO result VALUES(%Q);", name);
		sqlite3_exec(ppDb, sql, NULL, NULL, &errmsg);
		sqlite3_free(sql);

		sql = sqlite3_mprintf("UPDATE node SET win = 0 WHERE win = %d;", win);
		sqlite3_exec(ppDb, sql, NULL, NULL, &errmsg);
		sqlite3_free(sql);

		sql = sqlite3_mprintf("DELETE FROM node WHERE rowid = %d;", win);
		sqlite3_exec(ppDb, sql, NULL, NULL, &errmsg);
		sqlite3_free(sql);

		gtk_list_store_clear(GTK_LIST_STORE(tree_model));

		sql = "SELECT rowid, * FROM node WHERE win = 0;";
		sqlite3_exec(ppDb, sql, select_callback, tree_model, &errmsg);

		select_random_node(tree_model);
	}
}

G_MODULE_EXPORT void signal_clicked_button_left(GtkButton *button, gpointer user_data) {
	process_node(tree_view_left, tree_view_right);
}

G_MODULE_EXPORT void signal_clicked_button_right(GtkButton *button, gpointer user_data) {
	process_node(tree_view_right, tree_view_left);
}

static void select_random_node(GtkTreeModel* tree_model) {
	gint length = gtk_tree_model_iter_n_children(tree_model, NULL);
	if (length > 1) {
		gint32 random_left = g_random_int_range(0, length);
		gint32 random_right = g_random_int_range(0, length - 1);
		if (random_right >= random_left) {
			random_right++;
		}
		GtkTreeIter iter;
		gtk_tree_model_iter_nth_child(tree_model, &iter, NULL, random_left);
		GtkTreeSelection* tree_selection_left = gtk_tree_view_get_selection(GTK_TREE_VIEW(tree_view_left));
		gtk_tree_selection_select_iter(tree_selection_left, &iter);

		gtk_tree_model_iter_nth_child(tree_model, &iter, NULL, random_right);
		GtkTreeSelection* tree_selection_right = gtk_tree_view_get_selection(GTK_TREE_VIEW(tree_view_right));
		gtk_tree_selection_select_iter(tree_selection_right, &iter);
	} else if (length == 1) {
		char* sql;
		gint win;
		gchar* name;
		GtkTreeIter iter;
		gtk_tree_model_iter_nth_child(tree_model, &iter, NULL, 0);
		gtk_tree_model_get(tree_model, &iter, 0, &win, 1, &name, -1);

		sql = sqlite3_mprintf("INSERT INTO result VALUES(%Q);", name);
		sqlite3_exec(ppDb, sql, NULL, NULL, &errmsg);
		sqlite3_free(sql);

		sql = sqlite3_mprintf("UPDATE node SET win = 0 WHERE win = %d;", win);
		sqlite3_exec(ppDb, sql, NULL, NULL, &errmsg);
		sqlite3_free(sql);

		sql = sqlite3_mprintf("DELETE FROM node WHERE rowid = %d;", win);
		sqlite3_exec(ppDb, sql, NULL, NULL, &errmsg);
		sqlite3_free(sql);

		gtk_list_store_clear(GTK_LIST_STORE(tree_model));

		sql = "SELECT rowid, * FROM node WHERE win = 0;";
		sqlite3_exec(ppDb, sql, select_callback, tree_model, &errmsg);

		select_random_node(tree_model);
	}
}

G_MODULE_EXPORT void signal_changed_tree_selection(GtkTreeSelection* tree_selection, gpointer user_data) {
	GtkTreeModel* model;
	GtkTreeIter iter;
	gint rowid;
	gchar* name;
	gint win;
	if (gtk_tree_selection_get_selected(tree_selection, &model, &iter)) {
		gtk_tree_model_get(model, &iter, 0, &rowid, 1, &name, 2, &win, -1);
		GdkPixbuf* pixbuf = gdk_pixbuf_new_from_file_at_scale(name, 300, 300, TRUE, NULL);
		gtk_image_set_from_pixbuf(GTK_IMAGE(user_data), pixbuf);
		g_print("changed %d %s %s %d\n", rowid, g_path_get_dirname(name), g_path_get_basename(name), win);
	}
}

G_MODULE_EXPORT void signal_select_folder(GtkFileChooserButton *button, gpointer user_data) {
	gchar* folder = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(button));
	const gchar* filename;
	gchar* utf8_path;
	GtkListStore* list_store = GTK_LIST_STORE(user_data);
	GtkTreeIter iter;
	char* sql;
	GDir* dir = g_dir_open(folder, 0, NULL);
	gchar* opsysstring;
	if (dir) {
		while ((filename = g_dir_read_name(dir)) != NULL) {
			opsysstring = g_build_filename(folder, filename, NULL);
			utf8_path = g_locale_to_utf8(opsysstring, -1, NULL, NULL, NULL);
			sqlite3_free(errmsg);
			sql = sqlite3_mprintf("INSERT INTO node VALUES(%Q, 0);", opsysstring);
			sqlite3_exec(ppDb, sql, NULL, NULL, &errmsg);
			int rowid = (int) sqlite3_last_insert_rowid(ppDb);

			gtk_list_store_append(list_store, &iter);
			gtk_list_store_set(list_store, &iter, 0, rowid, 1, utf8_path, 2, 0, -1);

			sqlite3_free(sql);
			g_free(opsysstring);
		}
		select_random_node(GTK_TREE_MODEL(list_store));
		g_dir_close(dir);
	}
}

int select_callback(void *arg, int argc, char **argv, char **column) {
	gchar* utf8_path = g_locale_to_utf8((gchar *) *(argv + 1), -1, NULL, NULL, NULL);

	GtkListStore* list_store = GTK_LIST_STORE(arg);
	GtkTreeIter iter;
	gtk_list_store_append(list_store, &iter);
	gtk_list_store_set(list_store, &iter, 0, atoi((gchar *) *(argv)), 1, utf8_path, 2, atoi((gchar *) *(argv + 2)), -1);

	return SQLITE_OK;
}

int main(int argc, char *argv[]) {
	const char* create_sql = "CREATE TABLE IF NOT EXISTS node(name TEXT, win INTEGER);CREATE TABLE IF NOT EXISTS result(name TEXT);";
	const char* select_sql = "SELECT rowid, * FROM node WHERE win = 0;";

	gtk_init(&argc, &argv);
	
	GtkBuilder* builder = gtk_builder_new();
	gtk_builder_add_from_file(builder, "glade/tournament.glade", NULL);

	GtkWidget* window = GTK_WIDGET(gtk_builder_get_object(builder, "window"));
	
	GtkListStore* list_store = GTK_LIST_STORE(gtk_builder_get_object(builder, "list_store"));
	GtkCellRenderer* cell_renderer = gtk_cell_renderer_text_new();

	tree_view_left = GTK_WIDGET(gtk_builder_get_object(builder, "tree_view_left"));
	GtkTreeViewColumn* tree_view_column_left = gtk_tree_view_column_new_with_attributes("name", cell_renderer, "text", 1, NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(tree_view_left), tree_view_column_left);

	tree_view_right = GTK_WIDGET(gtk_builder_get_object(builder, "tree_view_right"));
	GtkTreeViewColumn* tree_view_column_right = gtk_tree_view_column_new_with_attributes("name", cell_renderer, "text", 1, NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(tree_view_right), tree_view_column_right);

	gtk_builder_connect_signals(builder, NULL);

	sqlite3_open_v2("save.sqlite3", &ppDb, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, NULL);
	sqlite3_exec(ppDb, create_sql, NULL, NULL, &errmsg);
	sqlite3_exec(ppDb, select_sql, select_callback, list_store, &errmsg);

	select_random_node(GTK_TREE_MODEL(list_store));

	gtk_widget_show_all(window);
	
	gtk_main();
	
	sqlite3_close(ppDb);

	return 0;
}
