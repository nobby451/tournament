# トーナメントビューワー（仮）

手持ちの画像を比較して順位付けをするための支援ツールです。

## Ubuntu 12.10でのビルド方法

    $ sudo apt-get install git libgtk-3-dev libsqlite3-dev
    $ git clone https://nobby451@bitbucket.org/nobby451/tournament.git
    $ cd tournament
    $ gcc -Wall -o tournament src/main.c `pkg-config --cflags --libs gtk+-3.0 gmodule-2.0 sqlite3`

## Windows 7でのビルド方法

### Git for Windowsのインストール

<http://code.google.com/p/msysgit/downloads/list>

上記のサイトより、最新版のGit for Windowsをダウンロードしてインストールします。

インストールオプションはデフォルトのまま変更しなくても構いません。

インストールが終了したら、Git Bashを起動して下記のコマンドを実行します。

    $ git clone https://nobby451@bitbucket.org/nobby451/tournament.git

### MinGWのインストール

<http://sourceforge.net/projects/mingw/files/>

上記のサイトより、最新版のmingw-get-instをダウンロードしてインストールします。

インストールオプションは最低でも下記のものを指定します。

* Download latest repository cataloguesを選択
* C Compilerをチェック（デフォルトでそうなっています）
* C++ Compilerをチェック（SQLiteのビルドに必要）
* MSYS Basic Systemをチェック

### GTK+3.0開発用パッケージのインストール

<http://sourceforge.jp/projects/sfnet_gtk-mingw/releases/>

上記のサイトより、最新版のGTK+ Bundleをダウンロードします。

MinGWをインストールしたフォルダに統合する形で全て上書き解凍します。

（例えば、C:\MinGWにインストールしたなら、C:\MinGW\binとアーカイブ直下のbinが統合されるように）

### SQLiteのビルド

<http://sqlite.org/download.html>

上記のサイトより、autoconf版のソースをダウンロードして、適当な場所に解凍します。

MinGW Shellを起動して、ソースを解凍したフォルダに移動し、下記のコマンドを実行します。

    $ ./configure --prefix=/mingw
    $ make
    $ make install

### トーナメントビューワーのビルド

下記のコマンドを実行します。

    $ cd ~/tournament
    $ gcc -Wall -o tournament.exe src/main.c `pkg-config --cflags --libs gtk+-3.0 sqlite3`
